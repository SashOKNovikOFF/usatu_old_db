﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OlympicGamesDB
{
    public partial class ReadCity : Form
    {
        public ReadCity()
        {
            InitializeComponent();
        }

        private void ReadCity_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'olympicGamesDataSet.Город' table. You can move, or remove it, as needed.
            this.cityTableAdapter.Fill(this.olympicGamesDataSet.Город);
            // TODO: This line of code loads data into the 'olympicGamesDataSet.Страна' table. You can move, or remove it, as needed.
            this.countryTableAdapter.Fill(this.olympicGamesDataSet.Страна);

        }
    }
}
