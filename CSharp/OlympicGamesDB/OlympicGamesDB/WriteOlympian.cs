﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OlympicGamesDB
{
    public partial class WriteOlympian : Form
    {
        public WriteOlympian()
        {
            InitializeComponent();
        }

        private void WriteOlympian_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'olympicGamesDataSet.Вид_спорта' table. You can move, or remove it, as needed.
            this.STTableAdapter.Fill(this.olympicGamesDataSet.Вид_спорта);
            // TODO: This line of code loads data into the 'olympicGamesDataSet.Олимпиада' table. You can move, or remove it, as needed.
            this.olympiadTableAdapter.Fill(this.olympicGamesDataSet.Олимпиада);
            // TODO: This line of code loads data into the 'olympicGamesDataSet.Страна' table. You can move, or remove it, as needed.
            this.countryTableAdapter.Fill(this.olympicGamesDataSet.Страна);
            // TODO: This line of code loads data into the 'olympicGamesDataSet.Олимпиец' table. You can move, or remove it, as needed.
            this.olympianTableAdapter.Fill(this.olympicGamesDataSet.Олимпиец);

        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            this.olympianTableAdapter.Update(this.olympicGamesDataSet.Олимпиец);
        }
    }
}
