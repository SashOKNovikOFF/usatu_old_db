﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OlympicGamesDB
{
    public partial class ReadOlympian : Form
    {
        public ReadOlympian()
        {
            InitializeComponent();
        }

        private void ReadOlympian_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'olympicGamesDataSet.Вид_спорта' table. You can move, or remove it, as needed.
            this.STTableAdapter.Fill(this.olympicGamesDataSet.Вид_спорта);
            // TODO: This line of code loads data into the 'olympicGamesDataSet.Олимпиада' table. You can move, or remove it, as needed.
            this.olympiadTableAdapter.Fill(this.olympicGamesDataSet.Олимпиада);
            // TODO: This line of code loads data into the 'olympicGamesDataSet.Страна' table. You can move, or remove it, as needed.
            this.countryTableAdapter.Fill(this.olympicGamesDataSet.Страна);
            // TODO: This line of code loads data into the 'olympicGamesDataSet.Олимпиец' table. You can move, or remove it, as needed.
            this.olympianTableAdapter.Fill(this.olympicGamesDataSet.Олимпиец);

        }
    }
}
