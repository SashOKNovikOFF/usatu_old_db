﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OlympicGamesDB
{
    public partial class WriteCountry : Form
    {
        public WriteCountry()
        {
            InitializeComponent();
        }

        private void WriteCountry_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'olympicGamesDataSet.Страна' table. You can move, or remove it, as needed.
            this.countryTableAdapter.Fill(this.olympicGamesDataSet.Страна);

        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            this.countryTableAdapter.Update(this.olympicGamesDataSet.Страна);
        }
    }
}
