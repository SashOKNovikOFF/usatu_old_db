﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OlympicGamesDB
{
    public partial class WriteOS : Form
    {
        public WriteOS()
        {
            InitializeComponent();
        }

        private void WriteOS_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'olympicGamesDataSet.Место_проведения' table. You can move, or remove it, as needed.
            this.OSTableAdapter.Fill(this.olympicGamesDataSet.Место_проведения);
            // TODO: This line of code loads data into the 'olympicGamesDataSet.Город' table. You can move, or remove it, as needed.
            this.cityTableAdapter.Fill(this.olympicGamesDataSet.Город);
            // TODO: This line of code loads data into the 'olympicGamesDataSet.Олимпиада' table. You can move, or remove it, as needed.
            this.olympiadTableAdapter.Fill(this.olympicGamesDataSet.Олимпиада);
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            this.OSTableAdapter.Update(this.olympicGamesDataSet.Место_проведения);
        }
    }
}
