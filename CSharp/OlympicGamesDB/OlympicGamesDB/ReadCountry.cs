﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OlympicGamesDB
{
    public partial class ReadCountry : Form
    {
        public ReadCountry()
        {
            InitializeComponent();
        }

        private void ReadCountry_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'olympicGamesDataSet.Страна' table. You can move, or remove it, as needed.
            this.countryTableAdapter.Fill(this.olympicGamesDataSet.Страна);

        }
    }
}
