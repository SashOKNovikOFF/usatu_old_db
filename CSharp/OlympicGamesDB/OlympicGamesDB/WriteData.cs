﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OlympicGamesDB
{
    public partial class WriteData : Form
    {
        public WriteData()
        {
            InitializeComponent();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void countryButton_Click(object sender, EventArgs e)
        {
            WriteCountry form = new WriteCountry();
            form.ShowDialog();
        }

        private void olympiadButton_Click(object sender, EventArgs e)
        {
            WriteOlympiad form = new WriteOlympiad();
            form.ShowDialog();
        }

        private void cityButton_Click(object sender, EventArgs e)
        {
            WriteCity form = new WriteCity();
            form.ShowDialog();
        }

        private void OSButton_Click(object sender, EventArgs e)
        {
            WriteOS form = new WriteOS();
            form.ShowDialog();
        }

        private void STButton_Click(object sender, EventArgs e)
        {
            WriteST form = new WriteST();
            form.ShowDialog();
        }

        private void olympianButton_Click(object sender, EventArgs e)
        {
            WriteOlympian form = new WriteOlympian();
            form.ShowDialog();
        }
    }
}
