﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OlympicGamesDB
{
    public partial class WriteOlympiad : Form
    {
        public WriteOlympiad()
        {
            InitializeComponent();
        }

        private void WriteOlympiad_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'olympicGamesDataSet.Олимпиада' table. You can move, or remove it, as needed.
            this.olympiadTableAdapter.Fill(this.olympicGamesDataSet.Олимпиада);

        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            this.olympiadTableAdapter.Update(this.olympicGamesDataSet.Олимпиада);
        }
    }
}
