﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OlympicGamesDB
{
    public partial class ReadOlympiad : Form
    {
        public ReadOlympiad()
        {
            InitializeComponent();
        }

        private void ReadOlympiad_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'olympicGamesDataSet.Олимпиада' table. You can move, or remove it, as needed.
            this.olympiadTableAdapter.Fill(this.olympicGamesDataSet.Олимпиада);

        }
    }
}
