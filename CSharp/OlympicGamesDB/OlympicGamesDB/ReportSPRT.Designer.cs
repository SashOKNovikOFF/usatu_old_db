﻿namespace OlympicGamesDB
{
    partial class ReportSPRT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comboBox = new System.Windows.Forms.ComboBox();
            this.CNTRButton = new System.Windows.Forms.Button();
            this.allCNTRButton = new System.Windows.Forms.Button();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.saveButton = new System.Windows.Forms.Button();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.chooseLabel = new System.Windows.Forms.Label();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.STBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.olympicGamesDataSet = new OlympicGamesDB.OlympicGamesDataSet();
            this.STTableAdapter = new OlympicGamesDB.OlympicGamesDataSetTableAdapters.Вид_спортаTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.STBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.olympicGamesDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox
            // 
            this.comboBox.DataSource = this.STBindingSource;
            this.comboBox.DisplayMember = "Название";
            this.comboBox.FormattingEnabled = true;
            this.comboBox.Location = new System.Drawing.Point(12, 40);
            this.comboBox.Name = "comboBox";
            this.comboBox.Size = new System.Drawing.Size(228, 21);
            this.comboBox.TabIndex = 1;
            this.comboBox.ValueMember = "Название";
            // 
            // CNTRButton
            // 
            this.CNTRButton.Location = new System.Drawing.Point(260, 38);
            this.CNTRButton.Name = "CNTRButton";
            this.CNTRButton.Size = new System.Drawing.Size(238, 23);
            this.CNTRButton.TabIndex = 2;
            this.CNTRButton.Text = "Получить отчёт по выбранному виду спорта";
            this.CNTRButton.UseVisualStyleBackColor = true;
            this.CNTRButton.Click += new System.EventHandler(this.SPRTButton_Click);
            // 
            // allCNTRButton
            // 
            this.allCNTRButton.Location = new System.Drawing.Point(260, 67);
            this.allCNTRButton.Name = "allCNTRButton";
            this.allCNTRButton.Size = new System.Drawing.Size(238, 23);
            this.allCNTRButton.TabIndex = 3;
            this.allCNTRButton.Text = "Получить отчёт по всем видам спорта";
            this.allCNTRButton.UseVisualStyleBackColor = true;
            this.allCNTRButton.Click += new System.EventHandler(this.allSPRTButton_Click);
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(0, 0);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.Size = new System.Drawing.Size(684, 311);
            this.dataGridView.TabIndex = 0;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(260, 96);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(238, 23);
            this.saveButton.TabIndex = 4;
            this.saveButton.Text = "Сохранить полученный отчёт";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.chooseLabel);
            this.splitContainer.Panel1.Controls.Add(this.comboBox);
            this.splitContainer.Panel1.Controls.Add(this.saveButton);
            this.splitContainer.Panel1.Controls.Add(this.allCNTRButton);
            this.splitContainer.Panel1.Controls.Add(this.CNTRButton);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.dataGridView);
            this.splitContainer.Size = new System.Drawing.Size(684, 461);
            this.splitContainer.SplitterDistance = 146;
            this.splitContainer.TabIndex = 0;
            // 
            // chooseLabel
            // 
            this.chooseLabel.AutoSize = true;
            this.chooseLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chooseLabel.Location = new System.Drawing.Point(8, 9);
            this.chooseLabel.Name = "chooseLabel";
            this.chooseLabel.Size = new System.Drawing.Size(179, 20);
            this.chooseLabel.TabIndex = 0;
            this.chooseLabel.Text = "Выберите вид спорта:";
            // 
            // STBindingSource
            // 
            this.STBindingSource.DataMember = "Вид спорта";
            this.STBindingSource.DataSource = this.olympicGamesDataSet;
            // 
            // olympicGamesDataSet
            // 
            this.olympicGamesDataSet.DataSetName = "OlympicGamesDataSet";
            this.olympicGamesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // STTableAdapter
            // 
            this.STTableAdapter.ClearBeforeFill = true;
            // 
            // ReportSPRT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 461);
            this.Controls.Add(this.splitContainer);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1000, 700);
            this.MinimumSize = new System.Drawing.Size(700, 500);
            this.Name = "ReportSPRT";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Отчёт по видам спорта с итогами по медалям";
            this.Load += new System.EventHandler(this.ReportSPRT_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel1.PerformLayout();
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.STBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.olympicGamesDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox;
        private System.Windows.Forms.Button CNTRButton;
        private System.Windows.Forms.Button allCNTRButton;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.Label chooseLabel;
        private System.Windows.Forms.BindingSource STBindingSource;
        private OlympicGamesDataSet olympicGamesDataSet;
        private OlympicGamesDataSetTableAdapters.Вид_спортаTableAdapter STTableAdapter;
    }
}