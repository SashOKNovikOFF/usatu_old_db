﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OlympicGamesDB
{
    public partial class ReadData : Form
    {
        public ReadData()
        {
            InitializeComponent();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void countryButton_Click(object sender, EventArgs e)
        {
            ReadCountry form = new ReadCountry();
            form.ShowDialog();
        }

        private void olympiadButton_Click(object sender, EventArgs e)
        {
            ReadOlympiad form = new ReadOlympiad();
            form.ShowDialog();
        }

        private void cityButton_Click(object sender, EventArgs e)
        {
            ReadCity form = new ReadCity();
            form.ShowDialog();
        }

        private void OSButton_Click(object sender, EventArgs e)
        {
            ReadOS form = new ReadOS();
            form.ShowDialog();
        }

        private void STButton_Click(object sender, EventArgs e)
        {
            ReadST form = new ReadST();
            form.ShowDialog();
        }

        private void olympianButton_Click(object sender, EventArgs e)
        {
            ReadOlympian form = new ReadOlympian();
            form.ShowDialog();
        }
    }
}
