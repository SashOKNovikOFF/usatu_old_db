﻿namespace OlympicGamesDB
{
    partial class ReadData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ReadTitle = new System.Windows.Forms.Label();
            this.countryButton = new System.Windows.Forms.Button();
            this.olympiadButton = new System.Windows.Forms.Button();
            this.cityButton = new System.Windows.Forms.Button();
            this.OSButton = new System.Windows.Forms.Button();
            this.STButton = new System.Windows.Forms.Button();
            this.olympianButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ReadTitle
            // 
            this.ReadTitle.AutoSize = true;
            this.ReadTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ReadTitle.Location = new System.Drawing.Point(121, 9);
            this.ReadTitle.Name = "ReadTitle";
            this.ReadTitle.Size = new System.Drawing.Size(195, 20);
            this.ReadTitle.TabIndex = 1;
            this.ReadTitle.Text = "Таблицы для просмотра";
            // 
            // countryButton
            // 
            this.countryButton.Location = new System.Drawing.Point(26, 77);
            this.countryButton.Name = "countryButton";
            this.countryButton.Size = new System.Drawing.Size(165, 23);
            this.countryButton.TabIndex = 1;
            this.countryButton.Text = "Страны";
            this.countryButton.UseVisualStyleBackColor = true;
            this.countryButton.Click += new System.EventHandler(this.countryButton_Click);
            // 
            // olympiadButton
            // 
            this.olympiadButton.Location = new System.Drawing.Point(233, 77);
            this.olympiadButton.Name = "olympiadButton";
            this.olympiadButton.Size = new System.Drawing.Size(165, 23);
            this.olympiadButton.TabIndex = 2;
            this.olympiadButton.Text = "Олимпиады";
            this.olympiadButton.UseVisualStyleBackColor = true;
            this.olympiadButton.Click += new System.EventHandler(this.olympiadButton_Click);
            // 
            // cityButton
            // 
            this.cityButton.Location = new System.Drawing.Point(26, 123);
            this.cityButton.Name = "cityButton";
            this.cityButton.Size = new System.Drawing.Size(165, 23);
            this.cityButton.TabIndex = 3;
            this.cityButton.Text = "Города";
            this.cityButton.UseVisualStyleBackColor = true;
            this.cityButton.Click += new System.EventHandler(this.cityButton_Click);
            // 
            // OSButton
            // 
            this.OSButton.Location = new System.Drawing.Point(233, 123);
            this.OSButton.Name = "OSButton";
            this.OSButton.Size = new System.Drawing.Size(165, 23);
            this.OSButton.TabIndex = 4;
            this.OSButton.Text = "Места проведения Олимпиад";
            this.OSButton.UseVisualStyleBackColor = true;
            this.OSButton.Click += new System.EventHandler(this.OSButton_Click);
            // 
            // STButton
            // 
            this.STButton.Location = new System.Drawing.Point(26, 173);
            this.STButton.Name = "STButton";
            this.STButton.Size = new System.Drawing.Size(165, 23);
            this.STButton.TabIndex = 5;
            this.STButton.Text = "Виды спорта";
            this.STButton.UseVisualStyleBackColor = true;
            this.STButton.Click += new System.EventHandler(this.STButton_Click);
            // 
            // olympianButton
            // 
            this.olympianButton.Location = new System.Drawing.Point(233, 173);
            this.olympianButton.Name = "olympianButton";
            this.olympianButton.Size = new System.Drawing.Size(165, 23);
            this.olympianButton.TabIndex = 6;
            this.olympianButton.Text = "Олимпийцы";
            this.olympianButton.UseVisualStyleBackColor = true;
            this.olympianButton.Click += new System.EventHandler(this.olympianButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(134, 247);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(165, 23);
            this.exitButton.TabIndex = 7;
            this.exitButton.Text = "Выход";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // ReadData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 337);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.olympianButton);
            this.Controls.Add(this.STButton);
            this.Controls.Add(this.OSButton);
            this.Controls.Add(this.cityButton);
            this.Controls.Add(this.olympiadButton);
            this.Controls.Add(this.countryButton);
            this.Controls.Add(this.ReadTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReadData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Чтение данных из БД";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ReadTitle;
        private System.Windows.Forms.Button countryButton;
        private System.Windows.Forms.Button olympiadButton;
        private System.Windows.Forms.Button cityButton;
        private System.Windows.Forms.Button OSButton;
        private System.Windows.Forms.Button STButton;
        private System.Windows.Forms.Button olympianButton;
        private System.Windows.Forms.Button exitButton;

    }
}