﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OlympicGamesDB
{
    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        private void writeDataButton_Click(object sender, EventArgs e)
        {
            WriteData tempForm = new WriteData();
            tempForm.ShowDialog();
        }

        private void readDataButton_Click(object sender, EventArgs e)
        {
            ReadData tempForm = new ReadData();
            tempForm.ShowDialog();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void getCountryReport_Click(object sender, EventArgs e)
        {
            ReportCNTR tempForm = new ReportCNTR();
            tempForm.ShowDialog();
        }

        private void getSportTypeReport_Click(object sender, EventArgs e)
        {
            ReportSPRT tempForm = new ReportSPRT();
            tempForm.ShowDialog();
        }
    }
}
