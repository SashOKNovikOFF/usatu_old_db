﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OlympicGamesDB
{
    public partial class WriteST : Form
    {
        public WriteST()
        {
            InitializeComponent();
        }

        private void WriteST_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'olympicGamesDataSet.Вид_спорта' table. You can move, or remove it, as needed.
            this.STTableAdapter.Fill(this.olympicGamesDataSet.Вид_спорта);

        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            this.STTableAdapter.Update(this.olympicGamesDataSet.Вид_спорта);
        }
    }
}
