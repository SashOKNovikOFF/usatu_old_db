﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

using System.Data.OleDb;

namespace OlympicGamesDB
{
    public partial class ReportSPRT : Form
    {
        OleDbConnection Connection;
        OleDbCommand SQLCommand;
        DataTable reportTable;
        OleDbDataAdapter dataAdapter;

        string temp, temp01, temp02, temp11, temp12;


        string union, olympiadPlace, query;

        public ReportSPRT()
        {
            Connection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Application.StartupPath + "\\OlympicGames.accdb");
            SQLCommand = new OleDbCommand();
            reportTable = new DataTable();
            dataAdapter = new OleDbDataAdapter();
            
            temp = "SELECT [Олимпиец].[Имя], [Олимпиец].[Фамилия], [Олимпиец].[Пол], [Олимпиец].[Медаль], [Страна].[Название] AS [Страна], [Вид спорта].[Название] AS [Вид спорта], [Вид спорта].[Дисциплина], [Вид спорта].[Одиночная/командная игра], [Олимпиада].[Дата открытия] AS [Олимпиада] FROM [Страна] INNER JOIN ( ( [Олимпиец] INNER JOIN [Вид спорта] ON [Олимпиец].[ID_Вид спорта] = [Вид спорта].[ID_Вид спорта] ) INNER JOIN [Олимпиада] ON [Олимпиец].[ID_Олимпиада] = [Олимпиада].[ID_Олимпиада] ) ON [Страна].[ID_Страна] = [Олимпиец].[ID_Страна]";

            temp01 = "SELECT [Страна], [Вид спорта], [Дисциплина], [Олимпиада], [Медаль] FROM (" + temp + ") WHERE ([Одиночная/командная игра] = \"Командная\") GROUP BY [Страна], [Вид спорта], [Дисциплина], [Олимпиада], [Медаль]";
            temp02 = "SELECT [Страна], [Вид спорта], [Дисциплина], [Олимпиада], iif([Медаль] = \"Бронза\", 1, 0) AS [Бронза], iif([Медаль] = \"Серебро\", 1, 0) AS [Серебро], iif([Медаль] = \"Золото\", 1, 0) AS [Золото] FROM (" + temp01 + ") GROUP BY [Страна], [Вид спорта], [Дисциплина], [Олимпиада], [Медаль]";

            temp11 = "SELECT [Страна], [Вид спорта], [Дисциплина], [Олимпиада], iif([Медаль] = \"Бронза\", 1, 0) AS [Бронза], iif([Медаль] = \"Серебро\", 1, 0) AS [Серебро], iif([Медаль] = \"Золото\", 1, 0) AS [Золото] FROM (" + temp + ") WHERE ([Одиночная/командная игра] = \"Одиночная\") GROUP BY [Страна], [Вид спорта], [Дисциплина], [Олимпиада], [Медаль]";
            temp12 = "SELECT [Страна], [Вид спорта], [Дисциплина], [Олимпиада], sum([Бронза]) AS [Бронза], sum([Серебро]) AS [Серебро], sum([Золото]) AS [Золото] FROM (" + temp11 + ") GROUP BY [Страна], [Вид спорта], [Дисциплина], [Олимпиада]";

            union = "SELECT [Страна], [Вид спорта], [Дисциплина], [Олимпиада], [Бронза], [Серебро], [Золото] FROM (" + temp02 + ") UNION SELECT [Страна], [Вид спорта], [Дисциплина], [Олимпиада], [Бронза], [Серебро], [Золото] FROM (" + temp12 + ") ORDER BY [Страна], [Вид спорта], [Дисциплина], [Олимпиада]";
            olympiadPlace = "SELECT [Олимпиада].[Дата открытия] AS [Олимпиада], [Страна].[Название] AS [Страна проведения], [Город].[Название] AS [Город проведения] FROM [Страна] INNER JOIN ([Город] INNER JOIN ([Олимпиада] INNER JOIN [Место проведения] ON [Олимпиада].[ID_Олимпиада] = [Место проведения].[ID_Олимпиада]) ON [Город].[ID_Город] = [Место проведения].[ID_Город]) ON [Страна].[ID_Страна] = [Город].[ID_Страна]";
            query = "SELECT [Union].[Страна] AS [Страна-участница], [Вид спорта], [Дисциплина], [Union].[Олимпиада] AS [Олимпиада], [Страна проведения], [Город проведения], [Бронза], [Серебро], [Золото] FROM (" + union + ") AS [Union], (" + olympiadPlace + ") AS [Olympiad] WHERE ([Union].[Олимпиада] = [Olympiad].[Олимпиада])";

            InitializeComponent();
        }


        private void ReportSPRT_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "olympicGamesDataSet.Вид_спорта". При необходимости она может быть перемещена или удалена.
            this.STTableAdapter.Fill(this.olympicGamesDataSet.Вид_спорта);
        }

        private void SPRTButton_Click(object sender, EventArgs e)
        {
            reportTable.Clear();

            query = "SELECT [Вид спорта], [Union].[Олимпиада] AS [Дата проведения], [Страна проведения], [Город проведения], [Дисциплина], [Union].[Страна] AS [Страна-участница], [Бронза], [Серебро], [Золото] FROM (" + union + ") AS [Union], (" + olympiadPlace + ") AS [Olympiad] WHERE ([Union].[Олимпиада] = [Olympiad].[Олимпиада])";
            string CNTRquery = "SELECT * FROM (" + query + ") WHERE ([Вид спорта] = \"" + comboBox.SelectedValue + "\")";

            SQLCommand = new OleDbCommand(CNTRquery + ";", Connection);

            Connection.Open();
            SQLCommand.ExecuteNonQuery();
            dataAdapter = new OleDbDataAdapter(SQLCommand);
            dataAdapter.Fill(reportTable);
            dataGridView.DataSource = reportTable;
            Connection.Close();
        }

        private void allSPRTButton_Click(object sender, EventArgs e)
        {
            reportTable.Clear();

            query = "SELECT [Вид спорта], [Union].[Олимпиада] AS [Дата проведения], [Страна проведения], [Город проведения], [Дисциплина], [Union].[Страна] AS [Страна-участница], [Бронза], [Серебро], [Золото] FROM (" + union + ") AS [Union], (" + olympiadPlace + ") AS [Olympiad] WHERE ([Union].[Олимпиада] = [Olympiad].[Олимпиада])";
            query = "SELECT * FROM (" + query + ") ORDER BY [Вид спорта], [Дата проведения], [Дисциплина], [Страна-участница]";

            SQLCommand = new OleDbCommand(query + ";", Connection);

            Connection.Open();
            SQLCommand.ExecuteNonQuery();
            dataAdapter = new OleDbDataAdapter(SQLCommand);
            dataAdapter.Fill(reportTable);
            dataGridView.DataSource = reportTable;
            Connection.Close();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            Stream mainStream;
            saveFileDialog.Filter = "txt files (*.txt)|*.txt";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                if ((mainStream = saveFileDialog.OpenFile()) != null)
                {
                    StreamWriter writer = new StreamWriter(mainStream);
                    try
                    {
                        for (int i = 0; i < dataGridView.Columns.Count; i++)
                            writer.Write(dataGridView.Columns[i].Name.PadRight(30));

                        writer.WriteLine();
                        writer.WriteLine();

                        for (int i = 0; i < dataGridView.RowCount; i++)
                        {
                            for (int j = 0; j < dataGridView.ColumnCount; j++)
                                writer.Write(dataGridView.Rows[i].Cells[j].Value.ToString().PadRight(30));
                            writer.WriteLine();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        writer.Close();
                    }
                }
            }
        }
    }
}
