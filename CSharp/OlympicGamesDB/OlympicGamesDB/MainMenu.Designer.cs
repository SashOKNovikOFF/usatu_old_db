﻿namespace OlympicGamesDB
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainTitle = new System.Windows.Forms.Label();
            this.readDataButton = new System.Windows.Forms.Button();
            this.writeDataButton = new System.Windows.Forms.Button();
            this.getCountryReport = new System.Windows.Forms.Button();
            this.getSportTypeReport = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // MainTitle
            // 
            this.MainTitle.AutoSize = true;
            this.MainTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MainTitle.Location = new System.Drawing.Point(50, 10);
            this.MainTitle.Name = "MainTitle";
            this.MainTitle.Size = new System.Drawing.Size(286, 20);
            this.MainTitle.TabIndex = 0;
            this.MainTitle.Text = "База данных по Олимпийским играм";
            // 
            // readDataButton
            // 
            this.readDataButton.Location = new System.Drawing.Point(71, 124);
            this.readDataButton.Name = "readDataButton";
            this.readDataButton.Size = new System.Drawing.Size(238, 23);
            this.readDataButton.TabIndex = 2;
            this.readDataButton.Text = "Прочитать данные по Олимпийским играм";
            this.readDataButton.UseVisualStyleBackColor = true;
            this.readDataButton.Click += new System.EventHandler(this.readDataButton_Click);
            // 
            // writeDataButton
            // 
            this.writeDataButton.Location = new System.Drawing.Point(71, 77);
            this.writeDataButton.Name = "writeDataButton";
            this.writeDataButton.Size = new System.Drawing.Size(238, 23);
            this.writeDataButton.TabIndex = 1;
            this.writeDataButton.Text = "Внести данные по Олимпийским играм";
            this.writeDataButton.UseVisualStyleBackColor = true;
            this.writeDataButton.Click += new System.EventHandler(this.writeDataButton_Click);
            // 
            // getCountryReport
            // 
            this.getCountryReport.Location = new System.Drawing.Point(71, 174);
            this.getCountryReport.Name = "getCountryReport";
            this.getCountryReport.Size = new System.Drawing.Size(238, 23);
            this.getCountryReport.TabIndex = 3;
            this.getCountryReport.Text = "Получить отчёт по странам-участницам";
            this.getCountryReport.UseVisualStyleBackColor = true;
            this.getCountryReport.Click += new System.EventHandler(this.getCountryReport_Click);
            // 
            // getSportTypeReport
            // 
            this.getSportTypeReport.Location = new System.Drawing.Point(71, 224);
            this.getSportTypeReport.Name = "getSportTypeReport";
            this.getSportTypeReport.Size = new System.Drawing.Size(238, 23);
            this.getSportTypeReport.TabIndex = 4;
            this.getSportTypeReport.Text = "Получить отчёт по видам спорта";
            this.getSportTypeReport.UseVisualStyleBackColor = true;
            this.getSportTypeReport.Click += new System.EventHandler(this.getSportTypeReport_Click);
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(71, 274);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(238, 23);
            this.exitButton.TabIndex = 5;
            this.exitButton.Text = "Закончить работу с базой данных";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 361);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.getSportTypeReport);
            this.Controls.Add(this.getCountryReport);
            this.Controls.Add(this.writeDataButton);
            this.Controls.Add(this.readDataButton);
            this.Controls.Add(this.MainTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "MainMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Главное меню";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label MainTitle;
        private System.Windows.Forms.Button readDataButton;
        private System.Windows.Forms.Button writeDataButton;
        private System.Windows.Forms.Button getCountryReport;
        private System.Windows.Forms.Button getSportTypeReport;
        private System.Windows.Forms.Button exitButton;
    }
}